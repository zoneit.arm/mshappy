from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.template import loader
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt
import connections
from user import models
# Create your views here.
@csrf_protect
@csrf_exempt
def login(request):
    template = loader.get_template('login.html')
    sss = RequestContext(request)
    return render(request,'login.html',locals())

def register(request):
    template = loader.get_template('register.html')
    sss = RequestContext(request)
    return render(request, 'register.html', locals())

def logins(request):
    if request.method == "POST":
        check = models.Specs.objects.filter(username=request.POST["username"],password=request.POST["password"]).first()
        if check is not None:
            return redirect("/admin/")
    return redirect("/")

def registers(request):
    if request.method == "POST":
        check = models.Specs.objects.filter(username=request.POST["username"],).first()
        if check is None:
            userc = models.Specs(name=request.POST["name"],username=request.POST["username"],role="member",password=request.POST["password"],tel=request.POST["tel"])
            userc.save()
            return redirect("/admin/")
    return redirect("/")
