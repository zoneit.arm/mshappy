from django.db import models

# Create your models here.
class Specs(models.Model):
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    tel = models.CharField(max_length=10)
    role = models.CharField(max_length=20,default='member')
    created_at = models.DateTimeField(null=True,blank=True,auto_now_add=True)
    updated_at = models.DateTimeField(null=True,blank=True,auto_now=True)

